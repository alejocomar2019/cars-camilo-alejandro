import axios from 'axios'
// Create Read Update Delete (CRUD)
export const create = async (data, token) => {
  try {
    const request = {
      method: 'POST',
      headers: {
        Authorization: 'Bearer' + token
      },
      url: 'https://rickandmortyapi.com/api/character',
      data
    }
    await axios(request)
  } catch (e) {
    console.error(e)
  }
}

export const deleteItem = async (id, token) => {
  try {
    const request = {
      method: 'DELETE',
      headers: {
        Authorization: 'Bearer' + token
      },
      url: 'https://rickandmortyapi.com/api/character' + id
    }
    await axios(request)
  } catch (e) {
    console.error(e)
  }
}

// var, let y const
export const getAllCharacters = async function (paginator = {}, search = '') {
  try {
    const request = {
      method: 'GET',
      url: 'https://rickandmortyapi.com/api/character',
      params: {
        name: search,
        page: paginator.page || 1
      }
    }
    const response = await axios(request)
    return { results: response.data.results, count: response.data.info.count }
  } catch (e) {
    console.error(e)
    return { results: [], count: 0 }
  }
}

export const getSingleCharacter = async function (id) {
  try {
    const request = {
      method: 'GET',
      url: `https://rickandmortyapi.com/api/character/${id}`
    }
    const response = await axios(request)
    return response.data
  } catch (e) {
    console.error(e)
    return {}
  }
}

export const update = async (data, id, token) => {
  try {
    const request = {
      method: 'PUT',
      headers: {
        Authorization: 'Bearer' + token
      },
      url: 'https://rickandmortyapi.com/api/character' + id,
      data
    }
    await axios(request)
  } catch (e) {
    console.error(e)
  }
}
