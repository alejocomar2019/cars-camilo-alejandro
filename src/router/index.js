import Vue from 'vue'
import VueRouter from 'vue-router'
// Components
import Home from '@/views/Home.vue'
import Item from '@/views/Item.vue'
import RickAndMortyTable from '@/components/RickAndMortyTable'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/rick-and-morty-items',
    name: 'Rick and Morty Items',
    component: RickAndMortyTable
  },
  {
    path: '/rick-and-morty-items/:itemId',
    name: 'Item Details',
    component: Item
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
