import isNumber from 'is-number'
// return true if the value is valid else return false or message
const rules = {}

rules.email = value => {
  const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (!value) return true
  return pattern.test(value) || 'Correo inválido'
}

rules.isANumber = value => {
  if (!value) return true
  return isNumber(value) || 'Este campo debe ser un número'
}
rules.min8 = value => {
  return (value && value.length >= 8) || 'La contraseña debe tener minimo 8 carácteres'
}

rules.onlyNumbers = value => {
  const pattern = /^[0-9]+$/
  if (!value) return true
  return pattern.test(value) || 'Solo se permiten números'
}

rules.zipCode = value => {
  if (!value) return true
  const pattern = /^[0-9]{5}(?:-[0-9]{4})?$/
  return pattern.test(value) || 'zipcode invalido'
}

rules.required = value => {
  let success = 'Requerido'
  if (value) {
    if (Array.isArray(value)) {
      if (value.length > 0) success = true
    } else if (typeof value === 'object' && Object.keys(value).length > 0) {
      success = true
    } else if (typeof value === 'string' && value.length > 0 && value.trim().length > 0) {
      success = true
    } else if (typeof value === 'number') {
      success = true
    }
  }
  return success
}

rules.url = value => {
  // https://regexr.com/3e6m0
  // eslint-disable-next-line
  const pattern = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([-.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?|^((http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):[0-9]{4}\/*/
  if (value === '') return true
  return pattern.test(value) || 'Url inválida'
}

rules.minimumAmount = (value = 0, minimumAmount = -1) => {
  return parseFloat(value) >= minimumAmount
}

export default rules
