import { getAllCharacters } from '@/services/rickAndMorty'

const state = {
  userInfo: {
    name: 'Alejandro',
    lastName: 'Correa',
    age: 24,
    gender: 'Male'
  },
  token: 'dasdasdasd87s6d586das8d9q3ru9jvzd9v9wefh9dvncusdncusd',
  year: 2021,
  rickAndMortyItems: [],
  totalItems: 0
}

const mutations = {
  SET_YEAR (state, newYear) {
    state.year = newYear
  },
  SET_RICK_AND_MORTY_ITEMS (state, items) {
    state.rickAndMortyItems = items
  },
  SET_RICK_AND_MORTY_ITEMS_COUNT (state, count) {
    state.totalItems = count
  },
  SET_USER_INFO_PROPERTY (state, payload) {
    const { key, value } = payload
    state.userInfo[key] = value
  }
}

const actions = {
  async getRickAndMortyItems (vuexContext, payload) {
    try {
      const response = await getAllCharacters()
      const { results, count } = response
      vuexContext.commit('SET_RICK_AND_MORTY_ITEMS', results)
      vuexContext.commit('SET_RICK_AND_MORTY_ITEMS_COUNT', count)
    } catch (e) {
      vuexContext.commit('SET_RICK_AND_MORTY_ITEMS', [])
      vuexContext.commit('SET_RICK_AND_MORTY_ITEMS_COUNT', 0)
    }
  }
}

const getters = {
  doubleYear (state) {
    return state.year * 2
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
